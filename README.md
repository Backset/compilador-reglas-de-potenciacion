# Tema
Desarrollo de un compilador para la evaluación y solución de problemas de potenciación. 
# Justificación
Conocer las reglas de potenciación dentro de nuestra carrera es de vital importancia, ya que haciendo uso de estas se pueden realizar operaciones como sumas, restas, multiplicaciones y divisiones con potencias. Al momento de intentar resolver un determinado ejercicio, en el caso de no emplear correctamente las reglas de potenciación, se nos hará muy difícil dar con la solución. Por lo tanto, conocer estas reglas puede ayudar a llegar a un resultado más directo, sin la necesidad de ejecutar varios pasos.
La principal importancia del compilador radica en que, sin estos programas no existiría ninguna aplicación informática, ya que son la base de la programación en cualquier plataforma, pues su función principal es traducir el código fuente a un lenguaje mucho más sencillo y entendible por la máquina, informando al usuario si existen errores con el fin de ejecutar la aplicación sin problema, por lo cual este proyecto tiene el propósito de desarrollar un compilador capaz de analizar si los datos de entrada corresponden o no a una potencia, y aplicar la respectiva regla de potenciación para darle solución de una forma más rápida. Esto nos lleva como estudiantes a aplicar los conocimientos adquiridos en la materia de compiladores y comprender de mejor manera la estructura y el proceso de compilación durante las fases del análisis del programa fuente y la síntesis del programa objeto. Además, se podrá utilizar este compilador para procesos simulados matemáticos. 
# Objetivos
## Objetivos Generales
- Desarrollar un compilador para la evaluación y solución de problemas de potenciación
## Objetivo Específicos
- Determinar la gramática del compilador en base a las reglas de potenciación. 
- Construir el compilador realizando el análisis del programa fuente y la síntesis del programa objeto.
- Realizar pruebas y verificar los resultados emitidos por el compilador.
