package Controler;

import Exeptions.ErrorException;
import Model.Token;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LexicalAnalyzer {
    private final ArrayList<String> errors;
    private final ArrayList<Token> tokens;

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    private int index;
    private int state;
    private StringBuilder lexeme;

    public static void main(String[] args) {
        LexicalAnalyzer lA = new LexicalAnalyzer();
//        String source = new Scanner(System.in).nextLine();
        String source = "^ 9 89 9,5 + - -9 -99 / = ( ) a aa [ ] ; # *";
        lA.analyzer(source);
        System.out.println("-------- Corrida ---------");
        for (int i = 0; i < lA.getTokens().size(); i++) {
            System.out.println(lA.getTokens().get(i));
        }
        System.out.println("--- Errors ---");
        System.out.println(lA.getErrors().toString());

    }

    public LexicalAnalyzer() {
        this.errors = new ArrayList<>();
        this.tokens = new ArrayList<>();
        this.index = 0;
        this.state = 0;
        this.lexeme = new StringBuilder();
    }

    private void analyzer(String source) {
        String textClean = clean(source);
        System.out.println(textClean);
        for (; index < textClean.length(); index++) {
            char letter = textClean.charAt(index);
            switch (state) {
                case 0:
                    if (letter == '^') {
                        two(1, letter);
                    } else if (Character.isDigit((letter))) {
                        two(2, letter);
                    } else if (letter == '+') {
                        two(6, letter);
                    } else if (letter == '-') {
                        two(7, letter);
                    } else if (letter == '/') {
                        two(11, letter);
                    } else if (letter == '=') {
                        two(12, letter);
                    } else if (letter == '(') {
                        two(13, letter);
                    } else if (letter == ')') {
                        two(14, letter);
                    } else if (Character.isLetter((letter))) {
                        two(15, letter);
                    } else if (letter == '[') {
                        two(17, letter);
                    } else if (letter == ']') {
                        two(18, letter);
                    } else if (letter == ';') {
                        two(19, letter);
                    } else if (letter == '#') {
                        two(20, letter);
                    } else if (letter == '*') {
                        two(21, letter);
                    } else {
                        try {
                            error(" q0");
                            throw new ErrorException("Error no se conoce el termino");
                        } catch (ErrorException ex) {
                            Logger.getLogger(LexicalAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case 1:
                    one("POTENCIA");
                    break;
                case 2:
                    if (Character.isDigit(letter)) {
                        two(2, letter);
                    } else if (letter == ' ') {
                        two(3, letter);
                    } else if (letter == ',') {
                        two(4, letter);
                    } else {
                        try {
                            error(" q2");
                            throw new ErrorException("Error no se conoce el termino");
                        } catch (ErrorException ex) {
                            Logger.getLogger(LexicalAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case 3:
                    one("NUMERO");
                    break;
                case 4:
                    if (Character.isDigit(letter)) {
                        two(4, letter);
                    } else if (letter == ' ') {
                        two(5, letter);
                    } else {
                        try {
                            error(" q4");
                            throw new ErrorException("Error no se conoce el termino");
                        } catch (ErrorException ex) {
                            Logger.getLogger(LexicalAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case 5:
                    one("DECIMAL");
                    break;
                case 6:
                    one("SUMA");
                    break;
                case 7:
                    if (Character.isDigit(letter)) {
                        two(8, letter);
                    } else if (letter == ' ') {
                        two(10, letter);
                    } else {
                        try {
                            error(" q7 error");
                            throw new ErrorException("Error no se conoce el termino");
                        } catch (ErrorException ex) {
                            Logger.getLogger(LexicalAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case 8:
                    if (Character.isDigit(letter)) {
                        two(8, letter);
                    } else if (letter == ' ') {
                        two(9, letter);
                    } else {
                        try {
                            error(" q8");
                            throw new ErrorException("Error no se conoce el termino");
                        } catch (ErrorException ex) {
                            Logger.getLogger(LexicalAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case 9:
                    one("NEGATIVO");
                    break;
                case 10:
                    one("RESTA");
                    break;
                case 11:
                    one("DIVISION");
                    break;
                case 12:
                    one("IGUAL");
                    break;
                case 13:
                    one("INICIO_V");
                    break;
                case 14:
                    one("FIN_V");
                    break;
                case 15:
                    if (Character.isLetter((letter))) {
                        two(15, letter);
                    } else if (letter == ' ') {
                        two(16, letter);
                    } else {
                        try {
                            error(" q15");
                            throw new ErrorException("Error no se conoce el termino");
                        } catch (ErrorException ex) {
                            Logger.getLogger(LexicalAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case 16:
                    one("LETRA");
                    break;
                case 17:
                    one("INICIO_GRUPO");
                    break;
                case 18:
                    one("FIN_GRUPO");
                    break;
                case 19:
                    one("DELIMITADOR");
                    break;
                case 20:
                    one("RAIZ");
                    break;
                case 21:
                    one("MULTIPLICACION");
                    break;
            }
        }

    }

    private void error(String x) {
        this.state = 0;
        this.errors.add(x);
    }

    private void two(int state, char letter) {
        this.state = state;
        this.lexeme.append(letter);
    }

    private void one(String type) {
        this.tokens.add(new Token(type, this.lexeme));
        this.lexeme = new StringBuilder();
        this.state = 0;
        this.index--;
    }

    public static String clean(String text) {
        StringBuilder textClean = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            switch (letter) {
                case '\r':
                case '\t':
                case '\n':
                case '\b':
                case '\f':
                    break;
                default:
                    textClean.append(letter);
            }
        }
        textClean.append(" ");
        return textClean.toString();
    }
}
